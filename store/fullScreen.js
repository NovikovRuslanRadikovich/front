export const state = () => ({ isFullScreen: false })

export const mutations = {
  toggle(state, isFullScreen) {
    state.isFullScreen = isFullScreen
  }
}

export const actions = {
  enable({ commit }) {
    commit('toggle', true)
  },
  disable({ commit }) {
    commit('toggle', false)
  }
}
