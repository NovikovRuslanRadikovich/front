FROM node:10.15.1-alpine

ENV APP_DIR=/usr/src/app

RUN mkdir -p $APP_DIR
WORKDIR $APP_DIR

ADD package.json package.json
RUN npm install

ADD . .
RUN npm run build

EXPOSE 3000
CMD npm run start
