import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: '#00a3f5', // a color that is not in the material colors palette
    accent: '#efeff5',
    secondary: '#f9faf9',
    info: '#ebebeb',
    warning: colors.amber.base,
    error: '#ffdae0',
    success: colors.green.accent3
  }
})
